const concurrently = require('concurrently');

concurrently([
    {command: 'node ./config/dolar', nombre: 'dolar' },
    {command: 'node ./bin/www', nom: 'wop'},
], {
    prefix: 'name',
    killOthers: ['failure', 'success'],
    restartTries: 3,
}).then();
